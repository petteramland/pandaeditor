# pandaeditor
A game engine built on panda3d and python.  

To download and install:

    $ pip install git+https://github.com/pokepetter/pandaeditor.git

Dependencies:
  * panda3d
